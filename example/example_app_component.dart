import 'package:angular/angular.dart';
import 'package:angular_components/laminate/popup/module.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_autoform/auto_form_component.dart';
import 'package:ana_autoform/auto_form_entry.dart';
import 'example_data.dart';

@Component(
  selector: 'my-app',
  directives: [AutoFormComponent],
  //Blanket provider for all kind of Angular Component. VERY BAD. But too lazy to find the right one for each component.
  providers: [popupBindings],
  template: '''
    <h1>Auto Form Example</h1>

    <auto-form [entries]="entries"
           (changeSubmitted)="changed"
           (cancelled)="cancelled">
    </auto-form>
  ''',
)
class ExampleAppComponent implements OnInit {
  List<AutoFormEntry> entries;

/*  @Input()
  AutoFormComponent autoForm;*/

  @override
  void ngOnInit() {
    generateEntriesFromSchema(generateSchemaForCar(), generateCar());
  }

  void generateEntriesFromSchema(Schema schema, Propsy target) {
    entries = List<AutoFormEntry>();
    final suggestions = {'TEST':['a', 'b', 'c']};

    schema.rules.forEach((rule) {

      final name = rule.name;
      final entry = AutoFormEntry()
        ..id = name
        ..rule = rule
        ..originalValue = target.props[name]?.value
      //TODO: wire up suggestions
        ..suggestions = suggestions['TEST'];

      entries.add(entry);
    });

    //entries.sort((a, b) => a.rule.order.compareTo(b.rule.order));

  }

  void changed(Map<String, dynamic> changes) {
    print(changes);
  }

  void cancelled() {
    print('form cancelled');
  }
}
