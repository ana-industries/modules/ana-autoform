import 'package:ana_metadata/metadata.dart';

part 'example_data.g.dart';

abstract class Car implements Propsy, Built<Car, CarBuilder> {
  factory Car([updates(CarBuilder b)]) = _$Car;
  Car._();
}

Schema generateSchemaForCar() {
  return Schema((builder) => builder
    ..id = 'not important here'
    ..rules.add(Rule(RuleType.text, 'Name', characterLimit: 32))
    ..rules.add(Rule(RuleType.text, 'Manufacturer', characterLimit: 64))
    ..rules.add(Rule(RuleType.text, 'Review', characterLimit: 1024))
    ..rules.add(Rule(RuleType.number, 'Door', characterLimit: 0))
    ..rules.add(Rule(RuleType.toggle, 'Bio Fuel', characterLimit: 0))
    ..rules.add(Rule(RuleType.dropdown, 'TEST', characterLimit: 0)));
}

Car generateCar() {
  return Car((builder) => builder
    ..id = 'not important too'
    ..props['Name'] = JsonObject('Model 3')
    ..props['Manufacturer'] = JsonObject('Tesla')
    ..props['Review'] = JsonObject(
        '''Highs: Quick acceleration and nimble handling, real-world-usable driving range, lots of future-is-now technology.
Lows: Driving range doesn't hold up on the highway, stark interior layout.
Verdict: Similar driving range and semi-autonomous driving tech but at half the price of the Model S. ''')
    ..props['Door'] = JsonObject(4)
    ..props['Bio Fuel'] = JsonObject(false)
    ..props['TEST'] = JsonObject('c')

    );
}
