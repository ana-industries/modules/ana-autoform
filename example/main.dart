import 'package:angular/angular.dart';
import 'example_app_component.template.dart' as ng;

void main() {
  runApp(ng.ExampleAppComponentNgFactory);
}
