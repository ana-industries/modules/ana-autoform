## 0.0.7

- Conflict resolution
- Dropdown to show resolution options.

## 0.0.6

- Use new RuleType

## 0.0.5

- Click on tooltip will close and reopen it.

## 0.0.4

- Green/Red input style to indicate changes.
- Tooltip to show previous value.

## 0.0.2

- Updated example with latest ana-metadata change.

## 0.0.1

- Initial version
