# Automatic Generated Form

Power all the forms in ANA. Due to the undefined layout of user data (the reason ANA has ANA_metadata), we cannot hand craft beautiful forms. We always assume we do not know all the possible fields for each type of data.

So we use the `Rule` from ANA_metadata to help determine the type and size of each input, e.g. character limit to determine the size of the text box, should it be a toggle or drop down. Then we lay them down in a grid and hopefully they look good in all kind of combinations.

## Features

### Auto Form Component

`AutoFormComponent` take a list of `AutoFormEntry' as input and output the changes when button are clicked.

It does not care how's the original data looks like, it can be a class, or a aggregated list of entries from multiple document. It just take care of the list of entries pass to it.

### Auto Form Entry

`AutoFormEntry` carries the context and value for each field of a data. These entries are the only way to use `AutoFormComponent`. We try not to introduce more input.

### CSS

We copied Google's `mdc-layout-grid/mdc-layout-grid.css` style to lay down the grid of inputs. I am indifferent of it. But has a feeling we might be better off if we able to roll our own style.

## Development

### Git branches

`master` is protected and reserve for main application development.

All active development should happen on `dev` branch. You should always branch out from `dev` and create push request against the `dev`.

### Pub Packages and ANA Modules

We open sourced a few building blocks of ANA, and these projects are ANA Modules. The cost of this is a tedious packages management in the main application.

Developers went through the trial projects will not be foreign to ANA Modules. Just a rule of thumb, we should always use the git packages unless you are working on feature related to that module.

### Style guide

[Dart's official guide](https://dart.dev/guides/language/effective-dart)

### Code Formatting

For `.dart`, use `dartfmt`. It is a tool came with Dart SDK. Do this in your command line `dartfmt -w .`

For `.html` `.scss` `.yaml`, use [prettier](https://prettier.io/docs/en/index.html). Install it and type `prettier --write "**/*.html" "**/*.scss" "**/*.yaml"` in your command line.

Better, if your IDE support it, just install the extension of these and enabled 'format on save'. It will be easier.

### Rules

- Commit and push your branch to GitLab, often.
- Have fun.

## TODO

- Introduce a new `Rule` that make a field a dropdown instead of text field with suggestion. This is for "these are the only possible value" kind of situation.
- Some combination of entries are just outright ugly, e.g. 3 medium length follow by a long one, left blank at the second row. What can we do?
- Error checking and prevent submission is non-existent.
- The button's color class is missing in this project, should we get them here? Or roll our own style with mixin?
- Do we need test?
