import 'package:angular/angular.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_tooltip/material_tooltip.dart';

import 'auto_form_entry.dart';

@Component(
    selector: 'auto-form-tooltip',
    directives: [
      MaterialIconComponent,
      MaterialPaperTooltipComponent,
      ClickableTooltipTargetDirective,
    ],
    styleUrls: ['auto_form_component.scss.css'],
    templateUrl: 'auto_form_tooltip_component.html')
class AutoFormTooltipComponent {
  @Input()
  String message;
  @Input()
  FieldStatus status;

  get icon {
    switch (status) {
      case FieldStatus.newChange:
        return 'add_circle_outline';
        break;
      case FieldStatus.modified:
      case FieldStatus.deleted:
        return 'remove_circle_outline';
        break;
      default:
    }
  }
}
