import 'package:ana_metadata/metadata.dart';
export 'package:ana_metadata/metadata.dart';

enum FieldStatus {
  noChange,
  newChange,
  modified,
  deleted,
  conflicted,
  resolved
}

class AutoFormEntry {
  String id;
  Rule rule;
  dynamic originalValue;
  dynamic baseValue;
  dynamic theirValue;
  dynamic suggestions;
  bool disabled = false;
  FieldStatus status = FieldStatus.noChange;

  String toString() {
    return '$id $originalValue $rule';
  }
}
